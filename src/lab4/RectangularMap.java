package lab4;

import lab2.MoveDirection;
import lab2.Position;
import lab3.Car;

import java.util.LinkedList;
import java.util.List;

public class RectangularMap implements IWorldMap {

    private Integer width;
    private Integer height;
    public Position upperBound;
    public Position lowerBound;
    public List<Car> cars = new LinkedList<>();

    public RectangularMap(Integer width, Integer height) {
        this.width = width;
        this.height = height;
        this.upperBound = new Position(width-1,height-1);
        this.lowerBound = new Position(0,0);
    }

    public Position getUpperBound() {
        return upperBound;
    }

    public Position getLowerBound() {
        return lowerBound;
    }

    @Override
    public String toString(){
        return new MapVisualizer().dump(this, lowerBound, upperBound);
    }


    @Override
    public boolean canMoveTo(Position position) {
        return position.smaller(upperBound) && position.bigger(lowerBound);
    }

    @Override
    public boolean add(Car car) { //DODAC sprawdzenie krawędzi mapy
        if (!isOccupied(car.getPosition())) {
            cars.add(cars.size(),car);
            return true;
        }
        else
            return false;
    }

    @Override
    public void run(List<MoveDirection> directions) {
        for (int i=0;i<directions.size();i++){
            Car car = cars.get(i%cars.size());
            car.move(directions.get(i));
        }
    }

    @Override
    public boolean isOccupied(Position position) {
        for (Car cartmp : cars){
            if(cartmp.getPosition().equals(position))
                return true;
        }
        return  false;
    }

    @Override
    public Object objectAt(Position position) {
        if (isOccupied(position)){
            for (Car car1 : cars) {
                if (car1.getPosition().equals(position))
                    return car1;
            }
        }
        return null;
    }
}