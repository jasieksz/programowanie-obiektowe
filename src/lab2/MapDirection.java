package lab2;

public enum MapDirection {
    North,
    East,
    South,
    West;

    @Override
    public String toString() {
        switch (this) {
            case North:
                return "Północ";
            case South:
                return "Półudnie";
            case West:
                return "Zachód";
            case East:
                return "Wschód";
        }
        return null;
    }

    public MapDirection next() {
        return MapDirection.values()[(this.ordinal() + 1) % 4];
    }

    public MapDirection previous() {
        return MapDirection.values()[(this.ordinal() + 3) % 4];
    }

}