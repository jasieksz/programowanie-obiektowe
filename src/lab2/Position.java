package lab2;


public class Position {
    public final int x;
    public final int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    public Position getPosition() {
        return new Position(this.x,this.y);
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    public boolean smaller (Position p) {
        return x <= p.x && y <= p.y;
    }

    public boolean bigger (Position p) {
        return x >= p.x && y >= p.y;
    }

    public Position add (Position p) {
        return new Position(p.x+this.x, p.y+this.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }


}