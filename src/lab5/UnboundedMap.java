package lab5;

import lab2.MoveDirection;
import lab2.Position;
import lab3.Car;
import lab4.IWorldMap;
import lab4.MapVisualizer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.max;
import static java.lang.Integer.min;

public class UnboundedMap implements IWorldMap {

    private List<HayStack> hayStackList;
    //public List<Car> carList = new LinkedList<>();
    Map<Position,Car> carList = new HashMap<>();
    public List<Car> cars = new LinkedList<>();
    private Position minObject;
    private Position maxObject;

    public UnboundedMap(List<HayStack> hayStackList) {
        this.hayStackList = hayStackList;
    }

    @Override
    public String toString(){
        return new MapVisualizer().dump(this, minObject,maxObject);
    }

    @Override
    public boolean canMoveTo(Position position) {
        return !isOccupied(position);
    }

    @Override
    public boolean add(Car car) {
        if (!isOccupied(car.getPosition())) {
            carList.put(car.getPosition(),car);
            cars.add(car);
            return true;
        }
        else
            return false;
    }

    @Override
    public void run(List<MoveDirection> directions) {
        for (int i=0;i<directions.size();i++){
            Car car = cars.get(i%cars.size());
            car.move(directions.get(i));
        }
    }

    @Override
    public boolean isOccupied(Position position) {

        if(carList.containsKey(position))
            return true;

        for (HayStack haystacktmp : hayStackList){
            if(haystacktmp.getPosition().equals(position))
                return true;
        }

        return  false;
    }

    @Override
    public Object objectAt(Position position) {
        if (isOccupied(position)){
            if (carList.containsKey(position))
                return carList.get(position);

            for (HayStack hstack : hayStackList) {
                if (hstack.getPosition().equals(position))
                    return hstack;
            }
        }
        return null;
    }

    public void findBounds(){
        Integer xmax= -1000; Integer xmin= 1000;
        Integer ymax= -1000; Integer ymin= 1000;
        for (Car car : carList.values()){
            xmax = max(car.getPosition().x,xmax);
            xmin = min(car.getPosition().x,xmin);
            ymax = max(car.getPosition().y,ymax);
            ymin = min(car.getPosition().y,ymin);
        }
        for (HayStack hay : hayStackList){
            xmax = max(hay.getPosition().x,xmax);
            xmin = min(hay.getPosition().x,xmin);
            ymax = max(hay.getPosition().y,ymax);
            ymin = min(hay.getPosition().y,ymin);
        }

        minObject = new Position(xmin-3,ymin-3);
        maxObject = new Position(xmax+3,ymax+3);
    }
}