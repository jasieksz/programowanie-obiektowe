package lab5;

import lab2.Position;

public class HayStack {

    private final Position hayposition;

    public HayStack(Position hayposition) {
        this.hayposition = hayposition;
    }


    public Position getPosition(){
        return hayposition;
    }

    @Override
    public String toString() {
        return "S";
    }
}
