package lab3;

import java.util.ArrayList;
import java.util.List;
import lab2.MoveDirection;

public class OptionsParser {

    public List parse(String args[]) throws IllegalArgumentException{
        List<MoveDirection> result = new ArrayList<>(args.length);
            for (int i = 0; i < args.length; i++) {
                switch (args[i]) {
                    case ("f"):
                    case ("forward"):
                        result.add(MoveDirection.Forward);
                        break;
                    case ("b"):
                    case ("backward"):
                        result.add(MoveDirection.Backward);
                        break;
                    case ("r"):
                    case ("right"):
                        result.add(MoveDirection.Right);
                        break;
                    case ("l"):
                    case ("left"):
                        result.add(MoveDirection.Left);
                        break;
                    default:
                        throw new IllegalArgumentException(args[i] + " is invalid argument");
                }
            }

        return result;
    }
}