package lab3;

import lab2.MoveDirection;
import lab2.Position;
import lab4.RectangularMap;
import lab5.HayStack;
import lab5.UnboundedMap;

import java.util.LinkedList;
import java.util.List;

public class CarRuner {

    public static void run(String[] args) {
        try {
            //RectangularMap map = new RectangularMap(7,7);
            HayStack slm1 = new HayStack(new Position(3, 2));
            HayStack slm2 = new HayStack(new Position(3, 6));
            List<HayStack> sloma = new LinkedList<>();
            List<Car> carLinkedList = new LinkedList<>();
            sloma.add(slm1);
            sloma.add(slm2);
            UnboundedMap map2 = new UnboundedMap(sloma);
            Car car1 = new Car(map2, 2, 2);
            Car car2 = new Car(map2, 3, 3);
            OptionsParser parser = new OptionsParser();
            map2.add(car1);
            map2.add(car2);
            map2.add(new Car(map2, 2, 0));
            carLinkedList.add(car1);
            carLinkedList.add(car2);
            map2.findBounds();
            List directions = parser.parse(args);
            map2.run(directions);
            System.out.println(map2.toString());
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
