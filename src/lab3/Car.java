package lab3;

import lab2.MapDirection;
import lab2.MoveDirection;
import lab2.Position;
import lab4.IWorldMap;

public class Car {

    private Position position;
    private MapDirection direction;
    private IWorldMap iMap;

    public Car(IWorldMap map){
        this.iMap = map;

    }

    public Car(IWorldMap map, int x, int y) {
        this.position = new Position(x,y);
        this.direction = MapDirection.North;
        this.iMap = map;
    }

    @Override
    public String toString(){
        return "C";
    }

    public void move(MoveDirection direction){
        switch (direction) {
            case Forward:
                changePosition(direction);
                break;
            case Backward:
                changePosition(direction);
                break;
            case Right:
                this.direction = this.direction.next();
                break;
            case Left:
                this.direction = this.direction.previous();
                break;
        }
    }

    private void changePosition(MoveDirection direction) { //dodac metode position.update , zeby nie bylo this.position...
        int a;
        a = direction.equals(MoveDirection.Forward) ? 1 : -1;
        if (this.direction == MapDirection.North)
            updatePosition(new Position(0,a));
        if (this.direction == MapDirection.South)
            updatePosition(new Position(0,-a));
        if (this.direction == MapDirection.West)
            updatePosition(new Position(-a,0));
        if (this.direction == MapDirection.East)
            updatePosition(new Position(a,0));
    }

    private void updatePosition(Position vector){ // OGRANICZENIA na RÓG MAPY, dostęp do tej mapy
        if (iMap.canMoveTo(this.position.add(vector)))
            this.position = this.position.add(vector);
        else
            System.out.println("Koniec mapy, nie zmieniono położenia");
    }

    public Position getPosition(){
        return this.position;
    }


}